import { describe, it, expect, beforeEach } from 'vitest';
import Campaignifier from './campaignifier';

describe('Different URL variants', () => {
	it.each([
		['http://example.com', 'one', 'http://example.com/?mtm_campaign=one'],
		['http://example.com', 'two', 'http://example.com/?mtm_campaign=two'],
		['http://example.com/', 'one', 'http://example.com/?mtm_campaign=one'],
		['http://example.com/home.html', 'one', 'http://example.com/home.html?mtm_campaign=one'],
		['http://foo.com/home?search=yes', 'one', 'http://foo.com/home?search=yes&mtm_campaign=one']
	])('%s with campaign %s should be %s', (sourceUrl, campaign, expectedUrl) => {
		const camp = new Campaignifier();

		camp.campaign = campaign;
		const result = camp.campaignify(`<a href="${sourceUrl}">`);
		expect(result).toBe(`<a href="${expectedUrl}">`);
	});
});

describe('Different HTML variants', () => {
	let camp: Campaignifier;

	beforeEach(() => {
		camp = new Campaignifier();
		camp.campaign = 'one';
	});
	it('should process multiple URLs in a document', () => {
		const source = `Visit <a href="http://here.com/">our website</a> for <a href="http://there.com/">more</a>`;
		const expected = `Visit <a href="http://here.com/?mtm_campaign=one">our website</a> for <a href="http://there.com/?mtm_campaign=one">more</a>`;
		const result = camp.campaignify(source);
		expect(result).toBe(expected);
	});

	it('should process multiple identical URLs in a document', () => {
		const source = `Visit <a href="http://here.com/">our website</a> for <a href="http://here.com/">more</a>`;
		const expected = `Visit <a href="http://here.com/?mtm_campaign=one">our website</a> for <a href="http://here.com/?mtm_campaign=one">more</a>`;
		const result = camp.campaignify(source);
		expect(result).toBe(expected);
	});

	it('should skip if a campaign is already present', () => {
		const source = `<a href="http://here.com/?mtm_campaign=already">`;
		const expected = `<a href="http://here.com/?mtm_campaign=already">`;
		const result = camp.campaignify(source);
		expect(result).toBe(expected);
	});

	it('should skip if no campaign is defined', () => {
		camp.campaign = '';
		const source = `<a href="http://here.com/">`;
		const expected = `<a href="http://here.com/">`;
		const result = camp.campaignify(source);
		expect(result).toBe(expected);
	});
});

describe('Href attribute only', () => {
	let camp: Campaignifier;

	beforeEach(() => {
		camp = new Campaignifier();
		camp.campaign = 'one';
	});
	it('should only affect the href attribute', () => {
		const source = `<a href="http://here.com/">http://here.com</a>`;
		const expected = `<a href="http://here.com/?mtm_campaign=one">http://here.com</a>`;
		const result = camp.campaignify(source);
		expect(result).toBe(expected);
	});
});

describe('Limit to domains for which I run Matomo analytics', () => {
	it('should only campaignify links to configured domains', () => {
		const camp = new Campaignifier();
		camp.campaign = 'one';
		camp.linkDomains = ['example.com'];

		const source = `<a href="http://not.com/">One</a><a href="http://example.com/">Two</a>`;
		const expected = `<a href="http://not.com/">One</a><a href="http://example.com/?mtm_campaign=one">Two</a>`;
		const result = camp.campaignify(source);
		expect(result).toBe(expected);
	});
});
